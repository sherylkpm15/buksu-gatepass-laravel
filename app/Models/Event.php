<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_title',
        'date_started_at',
        'time_started_at',
        'date_ended_at',
        'time_ended_at',
        'restrictions',
        'status'
    ];
}
